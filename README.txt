
INTRODUCTION
------------

This module adds ARIA accessibility options to webforms.

FEATURES
--------

 * Provides the option of adding the jQuery validate library to all webforms, or the 
   module's default js file to add ARIA attributes.
 * Webform component ARIA labels
