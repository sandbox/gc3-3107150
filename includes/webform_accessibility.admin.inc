<?php

/**
 * Menu callback for admin/config/content/webform/webform_accessibility
 */
function webform_accessibility_admin_settings() {
  $form['validation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Validation Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['validation']['webform_accessibility_jquery_validate'] = array(
    '#type' => 'select',
    '#title' => t('Use jQuery Validate Plugin'),
    '#options' => array(
      0 => t('None'),
      1 => t('Webform Accessibility JS'),
      2 => t('jQuery Validate'),
      3 => t('All'),
    ),
    '#default_value' => variable_get('webform_accessibility_jquery_validate'),
    '#description' => t('Select to enable the jQuery Validate plugin for form accessibility.'),
  );
  
  $form['validation']['webform_accessibility_require_aria_labels'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require ARIA labels'),
    '#default_value' => variable_get('webform_accessibility_require_aria_labels'),
    '#description' => t('Select to require webform elements to set ARIA labels'),
  );
  
  $form['webform_accessibility_default_aria_labels'] = array(
    '#type' => 'textarea',
    '#format' => t('Plain text'),
    '#title' => t('Default ARIA labels'),
    '#description' => t('Add default ARIA labels available when editing form components. Separate each option with a comma.'),
    '#default_value' => variable_get('webform_accessibility_default_aria_labels'),
    '#after_build' => array('webform_accessibility_hide_format_info'),
  );

  $form = system_settings_form($form);
  $form['#theme'] = 'webform_accessibility_admin_settings';
  array_unshift($form['#submit'], 'webform_accessibility_admin_settings_submit');
  
  return $form;
}

/**
 * Post process callback for text_format
 */
function webform_accessibility_hide_format_info($form) {
  // Hide text format options
  $form['format']['#access'] = FALSE;
  return $form;
}

/**
 * Submit handler for webform_accessibility_admin_settings form
 */
function webform_accessibility_admin_settings_submit($form, &$form_state) {
  // Admin form submite handler
}
