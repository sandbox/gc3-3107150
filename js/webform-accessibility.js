jQuery(document).ready(function($) {
  // Custom validation script  
  $('.webform-client-form').attr('novalidate', 'novalidate');
  
  var checkIt = function ( e ) {
    var msg;

    if ( this.hasAttribute('data-msg') ) {
      msg = document.getElementById(this.getAttribute('data-error'));
    }

    if ( this.required && this.validity.valid ) {
      this.setAttribute('aria-invalid', 'false');
      msg.innerHTML = '';
      this.removeEventListener('keyup', checkIt, false);
    }
    else if ( this.required && !this.validity.valid  ) {
      this.setAttribute('aria-invalid', 'true');
      msg.innerHTML = this.getAttribute('data-msg');
      this.addEventListener('keyup', checkIt, false);
    }

  };

  var fields = document.querySelectorAll('.txt-field[required]');
  for ( var i = 0; i < fields.length; i++ ) {
    fields[i].addEventListener('blur', checkIt, false);
  }
  
  // Add aria labeledby tags
  $('.webform-component').each(function(){
    var itemlabel = $(this).find('label');
    var iteminput = $(this).find('input');
    if (itemlabel.attr('id') == undefined && iteminput.attr('id')) {
      inputid = iteminput.attr('id');
      itemlabel.attr('id', inputid + '-label');
      iteminput.attr('aria-labelledby', inputid + '-label');
    }
    else {
      inputid = iteminput.attr('id');
      iteminput.attr('aria-labelledby', inputid + '-label');
    }
  });
});
